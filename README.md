angular-syu
===========

The main angular module for all the filters, directives, services and controllers. :smile:


Installation
------------

    $ bower install angular-syu


Filters
-------

  * [Highlight](https://bitbucket.org/ShaneYu/angular-syu-highlight) -- `Performs highlighting based on string, array of strings or a function.`


Directives
----------

  _None as yet._


Controllers
-----------

  _None as yet._


Services
--------

  _None as yet._
  
  
License
-------

Licensed under the terms of the MIT License.