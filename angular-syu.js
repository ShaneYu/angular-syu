/**
 * Created by yus on 27/08/2014.
 *
 * AngularJS - SYU Module
 */

'use strict';

angular.module('syu.filters', []);
angular.module('syu.directives', []);
angular.module('syu.controllers', []);
angular.module('syu.services', []);

angular.module('syu', [
	'syu.filters',
	'syu.directives',
	'syu.controllers',
	'syu.services'
]);